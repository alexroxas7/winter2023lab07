public class Board{
	private Square[][] tictactoeBoard;
	
	public Board(){
		tictactoeBoard = new Square[3][3];
		
		for (int i = 0; i < tictactoeBoard.length; i++){
			for ( int j = 0; j < tictactoeBoard.length; j++){
				tictactoeBoard[i][j] = Square.BLANK;
			}
		}
	}
	
	public String toString(){
		String emptyString = "  ";
		// Adding coordinates to the board
		for (int k = 0; k < tictactoeBoard.length; k++){
			emptyString += k + " ";
		}
		emptyString += "\n";
		for (int i = 0; i < tictactoeBoard.length; i++){
			emptyString += i + " ";
			for ( int j = 0; j < tictactoeBoard.length; j++){
				emptyString += tictactoeBoard[i][j] + " ";
			}
			emptyString += "\n";
		}
		return emptyString;
	}
	
	public boolean placeToken(int row, int col, Square playerToken){
		if(row < 0 || col < 0 || col > 2 || row > 2 ){
			return false;
		} else if (tictactoeBoard[row][col] == Square.BLANK){
			tictactoeBoard[row][col] = playerToken;
			return true;
		} else {
			return false;
		}
	}
	
	public boolean checkIfFull(){
		for (int i = 0; i < tictactoeBoard.length; i++){
			for ( int j = 0; j < tictactoeBoard.length; j++){
				if(tictactoeBoard[i][j] == Square.BLANK){
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken){
		for (int i = 0; i < tictactoeBoard.length; i++){
			if (playerToken == tictactoeBoard[i][0] && playerToken == tictactoeBoard[i][1] && playerToken == tictactoeBoard[i][2]){
				return true;
			} 
		}	
		return false;
	}
	
	private boolean checkIfWinningVertical(Square playerToken){
		for (int i = 0; i < tictactoeBoard.length; i++){
			if (playerToken == tictactoeBoard[0][i] && playerToken == tictactoeBoard[1][i] && playerToken == tictactoeBoard[2][i]){
				return true;
			}
		}
		return false;
	}
	
	private boolean checkIfWinningDiagonal(Square playerToken){
		if(playerToken == tictactoeBoard[0][0] && playerToken == tictactoeBoard[1][1] && playerToken == tictactoeBoard[2][2]){
			return true;
		} else if (playerToken == tictactoeBoard[2][0] && playerToken == tictactoeBoard[1][1] && playerToken == tictactoeBoard[0][2]){
			return true;
		} else {
		return false;
		}
	}
	
	public boolean checkIfWinning(Square playerToken){
		if (checkIfWinningHorizontal(playerToken)){
			return true;
		} else if (checkIfWinningVertical(playerToken)){
			return true;
		} else if (checkIfWinningDiagonal(playerToken)){
			return true;
		} else {
			return false;
		}

	}
	
}
	
	

