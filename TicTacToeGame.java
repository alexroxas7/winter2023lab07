import java.util.Scanner;

public class TicTacToeGame{
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		int player1Wins = 0;
		int player2Wins = 0;
		boolean playAgain = true;
		
		while (playAgain){
			System.out.println("---Welcome to the Tic Tac Toe game!---");
			Board board = new Board();
			boolean gameOver = false;
			int player = 1;
			Square playerToken = Square.X;
			System.out.println("Player 1's token: X");
			System.out.println("Player 2's token: O");
			
			while (!gameOver){
				System.out.println(board);
				if (player == 1){
					playerToken = Square.X;
				} else {
					playerToken = Square.O;
				}
				System.out.println("Please input a number from 0-2 on which row you want to put your token("+ playerToken +"):");
				int row = sc.nextInt();
				System.out.println("Please input a number from 0-2 on which column you want to put your token("+ playerToken +"):");
				int col = sc.nextInt();
				boolean checkInvalid = board.placeToken(row,col,playerToken);
				if (!checkInvalid){
					while(!checkInvalid){
						System.out.println("The number(s) you have given was invalid \nPlease input a number from 0-2 on which row you want to put your token("+ playerToken +"):");
						row = sc.nextInt();
						System.out.println("Please input a number from 0-2 on which column you want to put your token("+ playerToken +"):");
						col = sc.nextInt();
						checkInvalid = board.placeToken(row,col,playerToken);
					}
				}
				if (board.checkIfFull()){
					System.out.println("Game Over! It's a tie!");
					gameOver = true;
					System.out.println("---FINAL BOARD---");
					System.out.println(board);
				} else if (board.checkIfWinning(playerToken)){
					System.out.println("Game Over! Player " + player + " has won");
					gameOver = true;
					System.out.println("---FINAL BOARD---");
					System.out.println(board);
					if (player == 1){
						player1Wins++;
					} else {
						player2Wins++;
					}
				} else {
					player++;
					if (player > 2){
						player = 1;
					}
				}
			}
			
			System.out.println("Would you like to play again? (Y/N)");
			String again = sc.next();
			if (again.equalsIgnoreCase("N")){
				System.out.println("Player 1 has won " + player1Wins + " games!");
				System.out.println("Player 2 has won " + player2Wins + " games!");
				playAgain = false;
			}
		}
	}
}